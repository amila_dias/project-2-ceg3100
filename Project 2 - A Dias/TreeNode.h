#pragma once
#include <string>

using namespace std;

class TreeNode
{
public:

	TreeNode::TreeNode()
	{
		title = "";
		name = "";
		parent = 0;
		lChild = 0;
		rSibling = 0;
		index = 0;
	}

	TreeNode::TreeNode(string title, string name, int parent, int lChild, int rSibling, int index)
	{
		this->title = title;
		this->name = name;
		this->parent = parent;
		this->lChild = lChild;
		this->rSibling = rSibling;
		this->index = index;
	}

	void setTitle(string title)
	{ 
		this->title = title;
	}
	string getTitle() 
	{ 
		return this->title;
	}

	void setName(string name)
	{
		this->name = name;
	}
	string getName() 
	{
		return this->name; 
	}

	void setParent(int parent) 
	{
		this->parent = parent; 
	}
	int getParent() 
	{
		return this->parent;
	}

	void setLChild(int lChild)
	{
		this->lChild = lChild;
	}
	int getLChild() 
	{
		return this->lChild; 
	}

	void setRSibling(int rSibling)
	{
		this->rSibling = rSibling;
	}
	int getRSibling()
	{
		return this->rSibling;
	}

	void setIndex(int index)
	{
		this->index = index; 
	}
	int getIndex() 
	{
		return this->index;
	}

	
private:
	string title;
	string name;
	int parent;
	int lChild;
	int rSibling;
	int index;

};




