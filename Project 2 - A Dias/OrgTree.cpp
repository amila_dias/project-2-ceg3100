#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "TreeNode.h"
#include "OrgTree.h"

#define	TREENODEPTR	unsigned int
#define	TREENULLPTR	-1

using namespace std;

string fileName;
ifstream infile;
int pubCounter;
int nodeCounter;
int maxItems = 10;
TreeNode *nodeArray;
vector<TreeNode> company;



void addRoot(string title, string name);
void readFile(string fileName);
unsigned int getSize();
void testPrint();
void checkArray();
void printSubTree(TreeNode subTreeRoot);
TREENODEPTR getRoot();
TREENODEPTR findEmployee(string title);


int main()
{
	//infile.open(fileName);
	//readFile(fileName);
	nodeArray = new TreeNode[maxItems];

	addRoot("test0", "test0");
	addRoot("test1", "Test1");
	addRoot("test2", "test2");
	addRoot("test3", "test3");
	addRoot("test4", "test4");
	addRoot("test5", "test5");
	addRoot("test6", "test6");
	addRoot("test7", "test7");
	addRoot("test8", "test8");
	addRoot("test9", "test9");


	cout << getRoot() << endl;
	cout << "Employee at: " << findEmployee("test4") << endl;


	testPrint();
	cout << "Size: " << getSize() << endl;

	system("pause");

	

	//infile.close();
	return 0;
}

void addRoot(string title, string name)
{
	if (nodeCounter == 0)
	{
		TreeNode node(title, name, -1, -1, -1, nodeCounter);
		nodeArray[0] = node;
		nodeCounter++;
	}
	else
	{
		TreeNode tempNode = nodeArray[0];
		tempNode.setParent(nodeCounter);
		nodeArray[0] = tempNode;
		TreeNode node(title, name, -1, tempNode.getIndex(), -1, nodeCounter);
		nodeCounter++;
		checkArray();
		for (int i = nodeCounter; i >=0; i--)
		{
			nodeArray[i + 1] = nodeArray[i];
		}
		nodeArray[0] = node;
		
	}
}

unsigned int getSize()
{
	unsigned int size = nodeCounter;
	return size;
}

TREENODEPTR getRoot()
{
	for (unsigned int i = 0; i < nodeCounter; i++)
	{
		if (nodeArray[i].getParent() == -1)
		{
			return nodeArray[i].getIndex();
		}
	}

	cout << "ERROR: ROOT NOT FOUND" << endl;
	return NULL;
}

TREENODEPTR findEmployee(string title)
{
	for (unsigned int i = 0; i < nodeCounter; i++)
	{
		if (nodeArray[i].getTitle().compare(title) == 0)
		{
			return nodeArray[i].getIndex();
		}
	}

	cout << "ERROR: EMPLOYEE NOT FOUND" << endl;
	return NULL;
}

TREENODEPTR leftMostChild(TreeNode treenode)
{
	return treenode.getLChild();
}

TREENODEPTR rightSibling(TreeNode treenode)
{
	return treenode.getRSibling();
}

void readFile(string fileName) 
{
	char c;
	string line;

	while (infile.eof() == 0)
	{
		c = infile.peek();
		getline(infile, line);
		if (c == ')')
		{
			pubCounter--;
		}
		else if (infile.eof())
		{
			cout << "end of file" << endl;
			break;
		}
		else
		{
			cout << "level: " << pubCounter << " " << line << endl;
			pubCounter++;
		}
	}
}

void printSubTree(TreeNode subTreeRoot)
{

}

void testPrint()
{
	for (unsigned int i = 0; i < nodeCounter; i++)
	{
		cout << "Title: " << nodeArray[i].getTitle()
			<< " Left Child: " << nodeArray[i].getLChild()
			<< " Parent " << nodeArray[i].getParent()
			<< " Index: " << nodeArray[i].getIndex() << endl;
	}
}

void checkArray()
{
	if (nodeCounter == maxItems-1)
	{
		maxItems *= 2;
		TreeNode* tempArray = new TreeNode[maxItems];
		for (int i = 0; i < nodeCounter + 1; i++)
		{
			tempArray[i] = nodeArray[i];
		}
		delete[] nodeArray;
		nodeArray = tempArray;
	}
}

