#pragma once
#define	TREENODEPTR	unsigned int
#define	TREENULLPTR -1
#include <string>

using namespace std;

class OrgTree
{
public:
	void addRoot(string title, string name);
	void readFile(string fileName);
	unsigned int getSize();
	void testPrint();
	void checkArray();

private:
	string fileName = "Test.txt";
	ifstream infile;
	int pubCounter = 0;
	int nodeCounter = 0;
	int maxItems;

};